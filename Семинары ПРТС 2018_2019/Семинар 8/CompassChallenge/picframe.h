#ifndef PICFRAME_H
#define PICFRAME_H

#include <QFrame>
#include <QPainter>


#include "ui_picframe.h"


class PicFrame : public QFrame, public Ui::PicFrame {
    Q_OBJECT

public:
    explicit PicFrame(QWidget *parent = 0);
    virtual ~PicFrame();

protected:
    //переопределяяем функцию paintEvent для отрисовки нашего безобразия
    void paintEvent(QPaintEvent *e);


private:
    float yaw; //значение курса аппарата
    float yawDesirable; //значение заданного курса аппарата
    //зададим координаты точек стрелки компаса
    //пусть центр стрелки находится в начале координат (0,0)
    QPoint arrowCompass[6]= {
                QPoint (0,50),
                QPoint (0,-50),
                QPoint (3,-40),
                QPoint (0,-50),
                QPoint (-3,-40),
                QPoint (0,-50)
            };
    //зададим координаты точек внешней стрелки
    //отвечающей за заданное положение ПА
    QPoint arrowDesirable [4] = {
        QPoint (0, -70),
        QPoint (5, -85),
        QPoint (-5, -85),
        QPoint (0,-70)
    };

public slots:
    void setYaw(int yaw);
    void setYawDesirable (int yaw);


    void setYaw(float yaw);
    void setYawDesirable (float yaw);

};

#endif // PICFRAME_H
