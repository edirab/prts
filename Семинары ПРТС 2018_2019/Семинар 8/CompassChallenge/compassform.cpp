#include "compassform.h"
#include <QDebug>
#include <QEvent>
#include <QKeyEvent>

CompassForm::CompassForm(QWidget *parent) :
    QWidget(parent) {

    setupUi(this);

    compass->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);

    connect(sldYawSet, SIGNAL(valueChanged(int)),
            lblSetYawValue,SLOT(setNum(int)));
    connect(sldYawCurrent, SIGNAL(valueChanged(int)),
            lblYawCurrentValue, SLOT(setNum(int)));
    connect (sldYawCurrent, SIGNAL(valueChanged(int)),
             compass, SLOT(setYaw(int)));
    connect (sldYawSet, SIGNAL(valueChanged(int)),
             compass, SLOT(setYawDesirable(int)));


}

CompassForm::~CompassForm() {

}

