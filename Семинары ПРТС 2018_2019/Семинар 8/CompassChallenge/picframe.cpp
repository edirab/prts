#include "picframe.h"
#include <QKeyEvent>
#include <QDebug>
#include <QMouseEvent>

PicFrame::PicFrame(QWidget *parent) :
    QFrame(parent) {
    setupUi(this);
    //проинициализируем 0 начальные значения курса ПА
    // и заданного курса
    yaw = 0;
    yawDesirable=0;

}

PicFrame::~PicFrame() {

}

void PicFrame::paintEvent(QPaintEvent *e){
    //При создании класса в качестве параметра передаем указатель
    //на тот объект, на котором мы рисуем
    QPainter painter(this);
    //настраиваем инструменты рисования (цвета,шрифты,толщины линий и прочее)
    QFont font;//создадим объект класса шрифта
    //включим технику сглаживания(Anti-aliasing), устраняющую ступенчатость рисунка
    painter.setRenderHint(QPainter::Antialiasing,true);
    //Транслируем систему координат так, что точка (0, 0) располагается в
    //центре виджета, вместо того чтобы быть в верхнем левом углу.
    //(функции width() и height() как раз возвращают нам значения ширины и длины для
    //виджета)
    painter.translate(width() / 2, height() / 2);
    //Мы также масштабируем систему координат на величину side / 100,
    //где side - ширина либо высота виджета, которая меньше по величине.
    //Мы хотим, чтобы часы были квадратными даже если устройство не квадратное.
    //Это даст нам квадратную область 200 x 200 с началом координат (0, 0) по центру,
    //в которой мы можем рисовать. То, что мы рисуем будет показано в наибольшем
    //возможном квадрате, который помещается в виджет.
    int side = qMin(width(), height());
    painter.scale(side / 200.0, side / 200.0);
    //мы закончили настройку и можем приступить к рисованию
    //чтобы сохранить текущие параметры (положения координат, масштаб,
    //а кроме того цвета, настройки пера и прочее вызываем метод save(),
    //который сохраняет все эти параметры в стэк)
    painter.save();
    //функция rotate() вращает нашу СК таким образом, что теперь направление оси Y повернуто
    // относительно вертикального для нас направления на угло yaw, переданный в качестве
    //параметра в эту функцию. А мы ранее задали точками стрелку такой, чтобы она
    //совпадала по направлению с осью OY
    painter.rotate(yaw);
    //Мы нарисовали стрелку компаса повернув систему координат и вызвав
    //QPainter::drawConvexPolygon(). Спасибо вращению, она нарисована в
    //правильном направлении.
    painter.drawConvexPolygon(arrowCompass, 6);
    painter.restore();
    //Используем функции save() и restore() чтобы сохранить и восстановить матрицу
    //преобразований до и после вращения, поскольку нам нужно разместить
    //стрелку заданного значения компаса без учёта любых предыдущих вращений.
   //снова сохраняем текущие параметры рисунка
    painter.save();
    //рисуем стрелку для вывода заданного значения курса
    painter.setPen(Qt::NoPen); //Перо можно устанавливать прозрачным Qt::NoPen
    //если нужно залить контур, но не нужно, чтобы контур был отрисован
    painter.setBrush(Qt::red); //Зададим цвет заливки сплошных контуров для
    //этой сессии рисования (работает между прошлым save() и следующим restore())
    painter.rotate (yawDesirable);//поворачиваем СК в соответствии с желаемым
    //значением курса
    //рисуем полигон методом drawConvexPolygon(массив с точками, кол-во точек на отрисовку);
    painter.drawConvexPolygon(arrowDesirable, 4);
    painter.restore();
    painter.save();
    //рисуем контурнашего компаса
    painter.drawEllipse(-60,-60,120,120);
    painter.drawEllipse(-70,-70,140,140);

    //рисуем шкалу компаса
    for (int j = 0; j < 60; ++j) {
        if ((j % 5) != 0)
            painter.drawLine(0, -55, 0, -60);
        else {
            painter.drawLine(0, -50, 0, -60);

            font.setPointSize(5);
            painter.setFont(font);
            painter.drawText(-20, -70, 40, 40,
                                         Qt::AlignHCenter | Qt::AlignTop,
                                         QString::number(j*6));
        }
            painter.rotate(6.0);
    }

    painter.setBrush(Qt::white);
    painter.drawRect(-15,-5,30,10);
    font.setPointSize(10);
    painter.drawText(QRect(-20,-10,40,20), Qt::AlignCenter, QString::number(yaw));
    painter.restore();
    painter.end();

}


void PicFrame::setYaw(float yawNew){
    yaw=yawNew;
    update();
}
void PicFrame::setYawDesirable(float yawDesirableNew){
    yawDesirable=yawDesirableNew;
    update();
}



void PicFrame::setYaw(int yawNew){
    yaw=yawNew;
    //благодаря вызову функции update() будет высылаться событие QPaintEvent
    //с виджета стирается все, что было нарисовано ранее и вызывается наша
    //функция paintEvent(), где как раз отрисовывается стрелка с новым значением yaw
    update();
}

void PicFrame::setYawDesirable(int yawNew){
    yawDesirable=yawNew;
    update();
}
