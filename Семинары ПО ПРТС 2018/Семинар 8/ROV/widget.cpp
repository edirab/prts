#include "widget.h"



Widget::Widget(QWidget *parent) :
    QFrame(parent)
{
    setupUi(this);
    su_rov = new SU_ROV ();
    buttonGroupMode->setExclusive(true); //кнопки для управления режимом СУ
                                         //можно объединить в группу QButtonGroup
    //это значительно упрощает программирование группы кнопок, в которой может быть
    //в один момент времени нажата только одна кнопка
    buttonGroupMode->setId(btnModeRuchnoi,SU_MODE::Ruchnoi);
    buttonGroupMode->setId(btnModeAuto, SU_MODE::Automatiz);
    connect(buttonGroupMode, SIGNAL(buttonToggled(int,bool)), su_rov, SLOT(changeMode(int,bool)));
    connect(btnKursPlus, SIGNAL(pressed()), su_rov, SLOT(addKurs()));
    connect(btnKursMinus, SIGNAL(pressed()), su_rov, SLOT(decKurs()));


}

Widget::~Widget()
{

}
