#ifndef WIDGET_H
#define WIDGET_H

#include <QFrame>
#include "su_rov.h"

#include "ui_controlFrame.h"

class Widget : public QFrame, Ui::ControlFrame
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:

    SU_ROV *su_rov;

};

#endif // WIDGET_H
