/*
    PIP - Platform Independent Primitives
    All includes
    Copyright (C) 2016  Ivan Pelipenko peri4ko@yandex.ru, Andrey Bychkov work.a.b@yandex.ru

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PIP_H
#define PIP_H

#include "picoremodule.h"
#include "picontainersmodule.h"
#include "piconsolemodule.h"
#include "pithreadmodule.h"
#include "piiomodule.h"
#include "pimathmodule.h"
#include "pisystemmodule.h"
#include "pigeomodule.h"

#endif // PIP_H
