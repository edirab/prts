/*! \file pimath.h
 * \brief Many mathematical functions and classes
*/
/*
	PIP - Platform Independent Primitives
	Many mathematical functions and classes
	Copyright (C) 2016  Ivan Pelipenko peri4ko@yandex.ru

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PIMATH_H
#define PIMATH_H

#include "pimathsolver.h"
#include "pistatistic.h"
#include "pifft.h"

#endif // PIMATH_H
