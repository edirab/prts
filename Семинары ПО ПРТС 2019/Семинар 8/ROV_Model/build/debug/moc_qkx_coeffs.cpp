/****************************************************************************
** Meta object code from reading C++ file 'qkx_coeffs.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.8.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../ROV_Model/qkx_coeffs.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qkx_coeffs.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.8.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Qkx_coeffs_t {
    QByteArrayData data[8];
    char stringdata0[78];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Qkx_coeffs_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Qkx_coeffs_t qt_meta_stringdata_Qkx_coeffs = {
    {
QT_MOC_LITERAL(0, 0, 10), // "Qkx_coeffs"
QT_MOC_LITERAL(1, 11, 11), // "sendSucceed"
QT_MOC_LITERAL(2, 23, 0), // ""
QT_MOC_LITERAL(3, 24, 10), // "sendFailed"
QT_MOC_LITERAL(4, 35, 14), // "receiveSucceed"
QT_MOC_LITERAL(5, 50, 13), // "receiveFailed"
QT_MOC_LITERAL(6, 64, 8), // "received"
QT_MOC_LITERAL(7, 73, 4) // "tick"

    },
    "Qkx_coeffs\0sendSucceed\0\0sendFailed\0"
    "receiveSucceed\0receiveFailed\0received\0"
    "tick"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Qkx_coeffs[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   44,    2, 0x06 /* Public */,
       3,    0,   45,    2, 0x06 /* Public */,
       4,    0,   46,    2, 0x06 /* Public */,
       5,    0,   47,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       6,    0,   48,    2, 0x08 /* Private */,
       7,    0,   49,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Qkx_coeffs::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Qkx_coeffs *_t = static_cast<Qkx_coeffs *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendSucceed(); break;
        case 1: _t->sendFailed(); break;
        case 2: _t->receiveSucceed(); break;
        case 3: _t->receiveFailed(); break;
        case 4: _t->received(); break;
        case 5: _t->tick(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Qkx_coeffs::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Qkx_coeffs::sendSucceed)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (Qkx_coeffs::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Qkx_coeffs::sendFailed)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (Qkx_coeffs::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Qkx_coeffs::receiveSucceed)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (Qkx_coeffs::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Qkx_coeffs::receiveFailed)) {
                *result = 3;
                return;
            }
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject Qkx_coeffs::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Qkx_coeffs.data,
      qt_meta_data_Qkx_coeffs,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Qkx_coeffs::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Qkx_coeffs::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Qkx_coeffs.stringdata0))
        return static_cast<void*>(const_cast< Qkx_coeffs*>(this));
    return QObject::qt_metacast(_clname);
}

int Qkx_coeffs::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 6)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 6;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 6)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void Qkx_coeffs::sendSucceed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, Q_NULLPTR);
}

// SIGNAL 1
void Qkx_coeffs::sendFailed()
{
    QMetaObject::activate(this, &staticMetaObject, 1, Q_NULLPTR);
}

// SIGNAL 2
void Qkx_coeffs::receiveSucceed()
{
    QMetaObject::activate(this, &staticMetaObject, 2, Q_NULLPTR);
}

// SIGNAL 3
void Qkx_coeffs::receiveFailed()
{
    QMetaObject::activate(this, &staticMetaObject, 3, Q_NULLPTR);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
