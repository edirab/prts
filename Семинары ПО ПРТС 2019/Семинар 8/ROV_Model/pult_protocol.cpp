#include "pult_protocol.h"
#include "qkx_coeffs.h"

Pult_protocol::Pult_protocol(const QString & config , const QString & name , QObject *parent) : QObject(parent) {

    QPIConfig conf(config, QIODevice::ReadOnly);
    QPIConfig::Entry & e(conf.getValue(name));
    ip_pult = e.getValue("sender.ip").value();
    ip_rov = e.getValue("receiver.ip").value();
    port_pult = e.getValue("sender.port", 0);
    port_rov = e.getValue("receiver.port", 0);
    frequency = e.getValue("receiver.frequency", 20);

    qDebug() << "Sender (Pult) ip:" << ip_pult << "port:" << port_pult;
    qDebug() << "Receiver (ROV) ip:" << ip_rov << "port:" << port_rov;
    qDebug() << "Frequency:" << frequency;

    timer = new QTimer(this);
    timer->start(1000/frequency);

    receiveSocket = new QUdpSocket(this);
    qDebug()<<receiveSocket->bind(ip_rov, port_rov);

    transmitSocket = new QUdpSocket(this);
    qDebug()<<"binded to ip:" << ip_rov <<"port:" << port_rov;

    connect(timer, SIGNAL(timeout()), SLOT(sendData()));
    connect(receiveSocket, SIGNAL(readyRead()),SLOT(receiveData()));

    to_pult.PSI=1;
    to_pult.GAMMA=2;
    to_pult.TETA=3;
    to_pult.VX=4;
    to_pult.VY=5;
    to_pult.VZ=6;
    to_pult.Wx=7;
    to_pult.Wy=8;
    to_pult.Wz=9;
}

uint Pult_protocol::checksum_i(const void * data, int size) { // function for checksum (uint)
    uint c = 0;
    for (int i = 0; i < size; ++i)
        c += ((const uchar*)data)[i];
    return ~(c + 1);
}

void Pult_protocol::sendData(){
    aboutSend();
   // qDebug() << to_pult.checksum;
    //qDebug() << validate(to_pult);
    transmitSocket->writeDatagram((char *)&to_pult, sizeof(to_pult),ip_pult, port_pult);
    qDebug() <<"send to ip:" << ip_pult << "port:" << port_pult;
}

void Pult_protocol::receiveData(){
    while(receiveSocket->hasPendingDatagrams()) {
        qDebug() <<"Received Vx" << from_pult.VX_dest;
        DestParam rec;
        receiveSocket->readDatagram((char *)&rec, sizeof(rec));
        if (!validate(rec)) {
            qDebug() << "Checksum validate" << validate(rec);
            continue;
        }
        from_pult = rec;
    }
}
