#ifndef CABLEPARAMS_H
#define CABLEPARAMS_H

#include "ui_cableparams.h"

class CableParams : public QWidget, private Ui::CableParams
{
    Q_OBJECT

public:
    explicit CableParams(QWidget *parent = nullptr);
    bool doublecable=0;
    double k1, k2, ksumm, kotr, kw1, kw2, diam, garazh, diam2;
private slots:
    void on_enable2_stateChanged(int arg1);
    void on_length1_editingFinished();
    void on_length2_editingFinished();
    void on_w1_editingFinished();
    void on_w2_editingFinished();
    void on_diametr_editingFinished();
    void on_length_editingFinished();
    void on_wgarazh_editingFinished();
    void on_diametr2_editingFinished();
};

#endif // CABLEPARAMS_H
