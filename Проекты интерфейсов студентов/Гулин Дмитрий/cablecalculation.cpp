#include "cablecalculation.h"
#include "buttons.h"
#include "cableparams.h"
#include "cableposition.h"

CableCalculation::CableCalculation(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    connect (widget_3, SIGNAL(button1_clicked()), this, SLOT(button1_clicked_1()));
    setWindowTitle("Расчет кабеля");
}

void CableCalculation::button1_clicked_1()
{
    CablePosition *window1 = new CablePosition;
    window1->en2=widget->doublecable;

    window1->l1=widget->k1;
    window1->diam=widget->diam;
    window1->w1= 9.81*widget->kotr*(1000*3.1416*pow(widget->diam,2)*0.000001/4-widget->kw1);

    window1->lotr=widget->kotr;

    window1->wgarazh = widget->garazh;

    window1->l2=widget->k2;
    window1->diam2 = widget->diam2;
    window1->w2=9.81*widget->kotr*(1000*3.1416*pow(widget->diam2,2)*0.000001/4-widget->kw2);

    window1->lsumm=widget->ksumm;

    window1->Px=widget_2->zPx;
    window1->Py=widget_2->zPy;
    window1->Px_max = widget_2->zPx_max;
    window1->Py_max = widget_2->zPy_max;
    if (fabs(window1->Px)>fabs(window1->Px_max))
        window1->Px_max = fabs(window1->Px*2);
    if (fabs(window1->Py)>fabs(window1->Py_max))
        window1->Py_max = fabs(window1->Py*2);
    if (window1->Px_max==0)
        window1->Px_max =10000;
    if (window1->Py_max==0)
        window1->Py_max =10000;
    window1->WTNPA=widget_2->zWTNPA;
    window1->Vx_TNPA=widget_2->zVx_TNPA;
    window1->Cvx1=widget_2->zCvx1;
    window1->Cvx2=widget_2->zCvx2;
    window1->max_Depth=widget_2->zDepth;
    window1->TitleSettings();
    window1->startCalculation();
    window1->show();
}
