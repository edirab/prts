/********************************************************************************
** Form generated from reading UI file 'tnpaparams.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TNPAPARAMS_H
#define UI_TNPAPARAMS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TNPAParams
{
public:
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *Px;
    QLabel *label_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_3;
    QLineEdit *Py;
    QLabel *label_4;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_9;
    QLineEdit *Px_max;
    QLabel *label_10;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_11;
    QLineEdit *Py_max;
    QLabel *label_12;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_13;
    QLineEdit *WTNPA;
    QLabel *label_14;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_15;
    QLineEdit *Cvx1;
    QLabel *label_16;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_17;
    QLineEdit *Cvx2;
    QLabel *label_18;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_19;
    QLineEdit *Vx;
    QLabel *label_20;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_21;
    QLineEdit *Max_depth;
    QLabel *label_22;

    void setupUi(QWidget *TNPAParams)
    {
        if (TNPAParams->objectName().isEmpty())
            TNPAParams->setObjectName(QStringLiteral("TNPAParams"));
        TNPAParams->resize(308, 539);
        TNPAParams->setMinimumSize(QSize(308, 510));
        verticalLayout_3 = new QVBoxLayout(TNPAParams);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox = new QGroupBox(TNPAParams);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setMinimumSize(QSize(291, 441));
        groupBox->setAlignment(Qt::AlignCenter);
        verticalLayout_4 = new QVBoxLayout(groupBox);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        groupBox_2->setMinimumSize(QSize(271, 121));
        verticalLayout_5 = new QVBoxLayout(groupBox_2);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(10);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(10, -1, 10, -1);
        label = new QLabel(groupBox_2);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        Px = new QLineEdit(groupBox_2);
        Px->setObjectName(QStringLiteral("Px"));
        Px->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(Px);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);


        verticalLayout_5->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(10);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(10, -1, 10, -1);
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_2->addWidget(label_3);

        Py = new QLineEdit(groupBox_2);
        Py->setObjectName(QStringLiteral("Py"));
        Py->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(Py);

        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_2->addWidget(label_4);


        verticalLayout_5->addLayout(horizontalLayout_2);


        verticalLayout_2->addWidget(groupBox_2);

        groupBox_3 = new QGroupBox(groupBox);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        groupBox_3->setMinimumSize(QSize(271, 121));
        verticalLayout_6 = new QVBoxLayout(groupBox_3);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(10);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(10, -1, 10, -1);
        label_9 = new QLabel(groupBox_3);
        label_9->setObjectName(QStringLiteral("label_9"));

        horizontalLayout_5->addWidget(label_9);

        Px_max = new QLineEdit(groupBox_3);
        Px_max->setObjectName(QStringLiteral("Px_max"));
        Px_max->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_5->addWidget(Px_max);

        label_10 = new QLabel(groupBox_3);
        label_10->setObjectName(QStringLiteral("label_10"));

        horizontalLayout_5->addWidget(label_10);


        verticalLayout_6->addLayout(horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(10);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(10, -1, 10, -1);
        label_11 = new QLabel(groupBox_3);
        label_11->setObjectName(QStringLiteral("label_11"));

        horizontalLayout_6->addWidget(label_11);

        Py_max = new QLineEdit(groupBox_3);
        Py_max->setObjectName(QStringLiteral("Py_max"));
        Py_max->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_6->addWidget(Py_max);

        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QStringLiteral("label_12"));

        horizontalLayout_6->addWidget(label_12);


        verticalLayout_6->addLayout(horizontalLayout_6);


        verticalLayout_2->addWidget(groupBox_3);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(10);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(10, -1, 10, -1);
        label_13 = new QLabel(groupBox);
        label_13->setObjectName(QStringLiteral("label_13"));

        horizontalLayout_7->addWidget(label_13);

        WTNPA = new QLineEdit(groupBox);
        WTNPA->setObjectName(QStringLiteral("WTNPA"));
        WTNPA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_7->addWidget(WTNPA);

        label_14 = new QLabel(groupBox);
        label_14->setObjectName(QStringLiteral("label_14"));

        horizontalLayout_7->addWidget(label_14);


        verticalLayout_2->addLayout(horizontalLayout_7);

        groupBox_4 = new QGroupBox(groupBox);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        groupBox_4->setMinimumSize(QSize(271, 121));
        verticalLayout_7 = new QVBoxLayout(groupBox_4);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(10);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        horizontalLayout_8->setContentsMargins(10, -1, 10, -1);
        label_15 = new QLabel(groupBox_4);
        label_15->setObjectName(QStringLiteral("label_15"));

        horizontalLayout_8->addWidget(label_15);

        Cvx1 = new QLineEdit(groupBox_4);
        Cvx1->setObjectName(QStringLiteral("Cvx1"));
        Cvx1->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_8->addWidget(Cvx1);

        label_16 = new QLabel(groupBox_4);
        label_16->setObjectName(QStringLiteral("label_16"));

        horizontalLayout_8->addWidget(label_16);


        verticalLayout_7->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(10);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        horizontalLayout_9->setContentsMargins(10, -1, 10, -1);
        label_17 = new QLabel(groupBox_4);
        label_17->setObjectName(QStringLiteral("label_17"));

        horizontalLayout_9->addWidget(label_17);

        Cvx2 = new QLineEdit(groupBox_4);
        Cvx2->setObjectName(QStringLiteral("Cvx2"));
        Cvx2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_9->addWidget(Cvx2);

        label_18 = new QLabel(groupBox_4);
        label_18->setObjectName(QStringLiteral("label_18"));

        horizontalLayout_9->addWidget(label_18);


        verticalLayout_7->addLayout(horizontalLayout_9);


        verticalLayout_2->addWidget(groupBox_4);


        verticalLayout_4->addLayout(verticalLayout_2);


        verticalLayout->addWidget(groupBox);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(10);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        horizontalLayout_10->setContentsMargins(10, -1, 10, -1);
        label_19 = new QLabel(TNPAParams);
        label_19->setObjectName(QStringLiteral("label_19"));

        horizontalLayout_10->addWidget(label_19);

        Vx = new QLineEdit(TNPAParams);
        Vx->setObjectName(QStringLiteral("Vx"));
        Vx->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_10->addWidget(Vx);

        label_20 = new QLabel(TNPAParams);
        label_20->setObjectName(QStringLiteral("label_20"));

        horizontalLayout_10->addWidget(label_20);


        verticalLayout->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(10);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(10, -1, 10, -1);
        label_21 = new QLabel(TNPAParams);
        label_21->setObjectName(QStringLiteral("label_21"));

        horizontalLayout_11->addWidget(label_21);

        Max_depth = new QLineEdit(TNPAParams);
        Max_depth->setObjectName(QStringLiteral("Max_depth"));
        Max_depth->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_11->addWidget(Max_depth);

        label_22 = new QLabel(TNPAParams);
        label_22->setObjectName(QStringLiteral("label_22"));

        horizontalLayout_11->addWidget(label_22);


        verticalLayout->addLayout(horizontalLayout_11);


        verticalLayout_3->addLayout(verticalLayout);


        retranslateUi(TNPAParams);

        QMetaObject::connectSlotsByName(TNPAParams);
    } // setupUi

    void retranslateUi(QWidget *TNPAParams)
    {
        TNPAParams->setWindowTitle(QApplication::translate("TNPAParams", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("TNPAParams", "\320\237\320\260\321\200\320\260\320\274\320\265\321\202\321\200\321\213 \320\242\320\235\320\237\320\220", nullptr));
        groupBox_2->setTitle(QApplication::translate("TNPAParams", "\320\241\320\270\320\273\321\213, \321\200\320\260\320\267\320\262\320\270\320\262\320\260\320\265\320\274\321\213\320\265 \320\224\320\240\320\232", nullptr));
        label->setText(QApplication::translate("TNPAParams", "Px", nullptr));
        label_2->setText(QApplication::translate("TNPAParams", "\320\235", nullptr));
        label_3->setText(QApplication::translate("TNPAParams", "Py", nullptr));
        label_4->setText(QApplication::translate("TNPAParams", "\320\235", nullptr));
        groupBox_3->setTitle(QApplication::translate("TNPAParams", "\320\241\320\270\320\273\321\213, \321\200\320\260\320\267\320\262\320\270\320\262\320\260\320\265\320\274\321\213\320\265 \320\224\320\240\320\232 (max)", nullptr));
        label_9->setText(QApplication::translate("TNPAParams", "Px (max)", nullptr));
        label_10->setText(QApplication::translate("TNPAParams", "\320\235", nullptr));
        label_11->setText(QApplication::translate("TNPAParams", "Py (max)", nullptr));
        label_12->setText(QApplication::translate("TNPAParams", "\320\235", nullptr));
        label_13->setText(QApplication::translate("TNPAParams", "\320\237\320\273\320\260\320\262\321\203\321\207\320\265\321\201\321\202\321\214 \320\242\320\235\320\237\320\220", nullptr));
        label_14->setText(QApplication::translate("TNPAParams", "\320\235", nullptr));
        groupBox_4->setTitle(QApplication::translate("TNPAParams", "\320\237\321\200\320\270\320\262\320\265\320\264\320\265\320\275\320\275\321\213\320\265 \320\263\320\270\320\264\321\200\320\276\320\264\320\270\320\275\320\260\320\274\320\270\321\207\320\265\321\201\320\272\320\270\320\265 \320\272\320\276\321\215\321\204\321\204\320\270\321\206\320\270\320\265\320\275\321\202\321\213", nullptr));
        label_15->setText(QApplication::translate("TNPAParams", "Cvx1", nullptr));
        label_16->setText(QApplication::translate("TNPAParams", "\320\235*\321\2012/\320\2742", nullptr));
        label_17->setText(QApplication::translate("TNPAParams", "Cvx2", nullptr));
        label_18->setText(QApplication::translate("TNPAParams", "\320\235*\321\201/\320\274", nullptr));
        label_19->setText(QApplication::translate("TNPAParams", "\320\241\320\272\320\276\321\200\320\276\321\201\321\202\321\214 \320\276\321\202\320\275\320\276\321\201\320\270\321\202\320\265\320\273\321\214\320\275\320\276 \320\262\320\276\320\264\321\213", nullptr));
        label_20->setText(QApplication::translate("TNPAParams", "\320\274/\321\201", nullptr));
        label_21->setText(QApplication::translate("TNPAParams", "\320\234\320\260\320\272\321\201\320\270\320\274\320\260\320\273\321\214\320\275\320\260\321\217 \320\263\320\273\321\203\320\261\320\270\320\275\320\260", nullptr));
        label_22->setText(QApplication::translate("TNPAParams", "\320\274", nullptr));
    } // retranslateUi

};

namespace Ui {
    class TNPAParams: public Ui_TNPAParams {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TNPAPARAMS_H
