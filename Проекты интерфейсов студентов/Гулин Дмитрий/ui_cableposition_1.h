/********************************************************************************
** Form generated from reading UI file 'cableposition_1.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CABLEPOSITION_1_H
#define UI_CABLEPOSITION_1_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_cableposition_1
{
public:
    QHBoxLayout *horizontalLayout_2;
    QLabel *L1label;
    QLabel *L2label;
    QLabel *Pxlabel;
    QLabel *Pylabel;
    QLabel *Vxlabel;

    void setupUi(QWidget *cableposition_1)
    {
        if (cableposition_1->objectName().isEmpty())
            cableposition_1->setObjectName(QStringLiteral("cableposition_1"));
        cableposition_1->resize(598, 45);
        cableposition_1->setMinimumSize(QSize(0, 45));
        cableposition_1->setMaximumSize(QSize(16777215, 45));
        cableposition_1->setStyleSheet(QStringLiteral("border: 1px solid black;"));
        horizontalLayout_2 = new QHBoxLayout(cableposition_1);
        horizontalLayout_2->setSpacing(30);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(20, 10, 20, 10);
        L1label = new QLabel(cableposition_1);
        L1label->setObjectName(QStringLiteral("L1label"));
        L1label->setMaximumSize(QSize(88, 16777215));
        L1label->setStyleSheet(QLatin1String("background-color:lightblue;\n"
"border: 1px solid grey;"));
        L1label->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(L1label);

        L2label = new QLabel(cableposition_1);
        L2label->setObjectName(QStringLiteral("L2label"));
        L2label->setMaximumSize(QSize(88, 16777215));
        L2label->setStyleSheet(QLatin1String("background-color:lightgreen;\n"
"border: 1px solid grey;"));
        L2label->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(L2label);

        Pxlabel = new QLabel(cableposition_1);
        Pxlabel->setObjectName(QStringLiteral("Pxlabel"));
        Pxlabel->setMaximumSize(QSize(88, 166666));
        Pxlabel->setStyleSheet(QLatin1String("background-color:white;\n"
"border: 1px solid grey;"));
        Pxlabel->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(Pxlabel);

        Pylabel = new QLabel(cableposition_1);
        Pylabel->setObjectName(QStringLiteral("Pylabel"));
        Pylabel->setMaximumSize(QSize(88, 16777215));
        Pylabel->setStyleSheet(QLatin1String("background-color:white;\n"
"border:1px solid grey;"));
        Pylabel->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(Pylabel);

        Vxlabel = new QLabel(cableposition_1);
        Vxlabel->setObjectName(QStringLiteral("Vxlabel"));
        Vxlabel->setMaximumSize(QSize(88, 16777215));
        Vxlabel->setStyleSheet(QLatin1String("background-color:white;\n"
"border:1px solid grey;"));
        Vxlabel->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(Vxlabel);


        retranslateUi(cableposition_1);

        QMetaObject::connectSlotsByName(cableposition_1);
    } // setupUi

    void retranslateUi(QWidget *cableposition_1)
    {
        cableposition_1->setWindowTitle(QApplication::translate("cableposition_1", "Form", nullptr));
        L1label->setText(QString());
        L2label->setText(QString());
        Pxlabel->setText(QString());
        Pylabel->setText(QString());
        Vxlabel->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class cableposition_1: public Ui_cableposition_1 {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CABLEPOSITION_1_H
