#include "tnpaparams.h"

TNPAParams::TNPAParams(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    zPx=zPy=zPx_max=zPy_max=zWTNPA=zCvx1=zCvx2=zVx_TNPA = zDepth =0;
}

void TNPAParams::on_Px_editingFinished()
{
    zPx=Px->text().toDouble();
}

void TNPAParams::on_Py_editingFinished()
{
    zPy=Py->text().toDouble();
}

void TNPAParams::on_Px_max_editingFinished()
{
    zPx_max=Px_max->text().toDouble();
}

void TNPAParams::on_Py_max_editingFinished()
{
    zPy_max=Py_max->text().toDouble();
}

void TNPAParams::on_WTNPA_editingFinished()
{
    zWTNPA=WTNPA->text().toDouble();
}

void TNPAParams::on_Cvx1_editingFinished()
{
    zCvx1=Cvx1->text().toDouble();
}

void TNPAParams::on_Cvx2_editingFinished()
{
    zCvx2=Cvx2->text().toDouble();
}

void TNPAParams::on_Max_depth_editingFinished()
{
    zDepth=Max_depth->text().toDouble();
}


void TNPAParams::on_Vx_editingFinished()
{
    zVx_TNPA = Vx->text().toDouble();
}
