#include "graphics.h"

Graphics::Graphics(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    NewGraphics();
}

void Graphics::NewGraphics()
{
    splineSeriesCable1 = new QSplineSeries;
    splineSeriesCable1->setPen(Qt::SolidLine);
    splineSeriesCable1->setColor(Qt::blue);
    splineSeriesCable2 = new QSplineSeries;
    splineSeriesCable2->setPen(Qt::SolidLine);
    splineSeriesCable2->setColor(Qt::green);
    splineSeriesTNPA = new QSplineSeries;
    splineSeriesTNPA->setPen(Qt::SolidLine);
    splineSeriesTNPA->setColor(Qt::red);
    lineSeriesShip = new QLineSeries;
    lineSeriesShip->setPen(Qt::SolidLine);
    lineSeriesShip->setColor(Qt::black);
    lineSeriesVx1 = new QLineSeries;
    lineSeriesVx2 = new QLineSeries;
    lineSeriesVx3 = new QLineSeries;
    lineSeriesVx4 = new QLineSeries;
    lineSeriesVx5 = new QLineSeries;
    lineSeries1 = new QLineSeries;
    lineSeries2 = new QLineSeries;
    lineSeriesVx1->setPen(Qt::SolidLine);
    lineSeriesVx1->setColor(Qt::blue);
    lineSeriesVx2->setPen(Qt::SolidLine);
    lineSeriesVx2->setColor(Qt::blue);
    lineSeriesVx3->setPen(Qt::SolidLine);
    lineSeriesVx3->setColor(Qt::blue);
    lineSeriesVx4->setPen(Qt::SolidLine);
    lineSeriesVx4->setColor(Qt::blue);
    lineSeriesVx5->setPen(Qt::SolidLine);
    lineSeriesVx5->setColor(Qt::blue);
    lineSeriesGarazh = new QLineSeries;
    lineSeriesGarazh->setPen(Qt::SolidLine);
    lineSeriesGarazh->setColor(Qt::black);
    lineSeriesGarazh->clear();
    splineSeriesCable1->clear();
    splineSeriesCable2->clear();
    splineSeriesTNPA->clear();
    lineSeriesShip->clear();
    lineSeriesVx1->clear();
    lineSeriesVx2->clear();
    lineSeriesVx3->clear();
    lineSeriesVx4->clear();
    lineSeriesVx5->clear();
    lineSeries1->clear();
    lineSeries2->clear();
    lineSeries1->setPen(Qt::DashDotLine);
    lineSeries1->setColor(Qt::black);
    lineSeries2->setPen(Qt::DashDotLine);
    lineSeries2->setColor(Qt::black);
    chart = new QChart;
    xAxis = new QValueAxis;
    yAxis = new QValueAxis;
    xAxis->setTitleText("X, м");
    yAxis->setTitleText("Y, м");
    xAxis->setTickCount(11);
    yAxis->setTickCount(11);
    chartView = new QChartView (chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    chart->addSeries(lineSeries1);
    chart->addSeries(lineSeries2);
    chart->addSeries(splineSeriesCable1);
    chart->addSeries(splineSeriesCable2);
    chart->addSeries(lineSeriesShip);
    chart->addSeries(splineSeriesTNPA);
    chart->addSeries(lineSeriesVx1);
    chart->addSeries(lineSeriesVx2);
    chart->addSeries(lineSeriesVx3);
    chart->addSeries(lineSeriesVx4);
    chart->addSeries(lineSeriesVx5);
    chart->addSeries(lineSeriesGarazh);
    hlay = new QHBoxLayout();
    hlay->setContentsMargins(0,0,0,0);
    hlay->addWidget(chartView);
    setLayout(hlay);
}

Graphics::~Graphics()
{

}

void Graphics::StartPaint()
{
    if (en2)
        Paint2();
    else
        Paint1();
}

void Graphics::Paint1()
{
    // установка осей
    if (fabs(x_min)>fabs(x_max))
        x_max = fabs(x_min);
    else x_max = fabs(x_max);
    if (fabs(y_min)>fabs(y_max))
        y_max = fabs(y_min);
    else y_max = fabs(y_max);
    yAxis->setRange(-y_max-50, 100);
    y_max+=100;
    if (x_max>(y_max/2))
    {
        xAxis->setRange(-x_max-50,x_max+50);
        lineSeries1->append(-x_max-50,0);
        lineSeries1->append(x_max+50,0);
        x_max+=50;
    }
    else
    {
        xAxis->setRange(-(y_max/2)-50, (y_max/2)+50);
        lineSeries1->append(-(y_max/2)-50,0);
        lineSeries1->append((y_max/2)+50,0);
        x_max = (y_max/2)+50;
    }

    chart->legend()->setVisible(false);
    chartView->chart()->setAxisX(xAxis, lineSeries1);
    chartView->chart()->setAxisY(yAxis, lineSeries1);
    lineSeries2->append(0,100);
    lineSeries2->append(0,-y_max-50);

    chartView->chart()->setAxisX(xAxis, lineSeries2);
    chartView->chart()->setAxisY(yAxis, lineSeries2);
    // конец настройки осей

    chartView->chart()->setAxisX(xAxis, splineSeriesCable1);
    chartView->chart()->setAxisY(yAxis, splineSeriesCable1);
    //получение данных с таблицы
    for (int i = 0; i<count1; i++)
    {
        en1 = 1;
        emit GetData(i);
        while (en1)
        {
            // задержка
        }
        splineSeriesCable1->append(X,Y);
    }
    // Рисуем корабль

    lineSeriesShip->append(0,0);
    lineSeriesShip->append(-(x_max/10),(y_max+100)/20);
    lineSeriesShip->append((3*x_max/10),(y_max+100)/20);
    lineSeriesShip->append((x_max/5),0);
    lineSeriesShip->append(0,0);

    chartView->chart()->setAxisX(xAxis, lineSeriesShip);
    chartView->chart()->setAxisY(yAxis, lineSeriesShip);
    //нарисовали корабль
    //Рисуем ТНПА

    splineSeriesTNPA->append(X, (Y+y_max/50));
    splineSeriesTNPA->append((X+x_max/20),(Y+y_max/80));
    splineSeriesTNPA->append((X+x_max/15),Y);
    splineSeriesTNPA->append((X+x_max/20),(Y-y_max/80));
    splineSeriesTNPA->append(X, (Y-y_max/50));
    splineSeriesTNPA->append((X-x_max/20),(Y-y_max/80));
    splineSeriesTNPA->append((X-x_max/15),Y);
    splineSeriesTNPA->append((X-x_max/20),(Y+y_max/80));
    splineSeriesTNPA->append(X, (Y+y_max/50));

    chartView->chart()->setAxisX(xAxis, splineSeriesTNPA);
    chartView->chart()->setAxisY(yAxis, splineSeriesTNPA);
    //Нарисовали корабль
    //Рисуем векторы скорости

    lineSeriesVx1->append(x_max, -(y_max/6));
    lineSeriesVx2->append(x_max, -(y_max/6)*2);
    lineSeriesVx3->append(x_max, -(y_max/6)*3);
    lineSeriesVx4->append(x_max, -(y_max/6)*4);
    lineSeriesVx5->append(x_max, -(y_max/6)*5);
    lineSeriesVx1->append(x_max-x_max*Vx/10, -(y_max/6));
    lineSeriesVx2->append(x_max-x_max*Vx/10, -(y_max/6)*2);
    lineSeriesVx3->append(x_max-x_max*Vx/10, -(y_max/6)*3);
    lineSeriesVx4->append(x_max-x_max*Vx/10, -(y_max/6)*4);
    lineSeriesVx5->append(x_max-x_max*Vx/10, -(y_max/6)*5);
    double deltax = x_max*Vx/20;
    double deltay = y_max/48;
    lineSeriesVx1->append(x_max-x_max*Vx/10+deltax, -(y_max/6)+deltay);
    lineSeriesVx2->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*2+deltay);
    lineSeriesVx3->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*3+deltay);
    lineSeriesVx4->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*4+deltay);
    lineSeriesVx5->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*5+deltay);
    lineSeriesVx1->append(x_max-x_max*Vx/10, -(y_max/6));
    lineSeriesVx2->append(x_max-x_max*Vx/10, -(y_max/6)*2);
    lineSeriesVx3->append(x_max-x_max*Vx/10, -(y_max/6)*3);
    lineSeriesVx4->append(x_max-x_max*Vx/10, -(y_max/6)*4);
    lineSeriesVx5->append(x_max-x_max*Vx/10, -(y_max/6)*5);
    lineSeriesVx1->append(x_max-x_max*Vx/10+deltax, -(y_max/6)-deltay);
    lineSeriesVx2->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*2-deltay);
    lineSeriesVx3->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*3-deltay);
    lineSeriesVx4->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*4-deltay);
    lineSeriesVx5->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*5-deltay);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx1);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx1);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx2);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx2);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx3);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx3);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx4);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx4);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx5);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx5);
    //Написовали стрелки
    emit SendEnd(0); //Сигнал о завершении рисования
}

void Graphics::Paint2()
{
    // установка осей
    if (fabs(x_min)>fabs(x_max))
        x_max = fabs(x_min);
    else x_max = fabs(x_max);
    if (fabs(y_min)>fabs(y_max))
        y_max = fabs(y_min);
    else y_max = fabs(y_max);
    yAxis->setRange(-y_max-50, 100);
    if (x_max>(y_max/2))
    {
        xAxis->setRange(-x_max-50,x_max+50);
        lineSeries1->append(-x_max-50,0);
        lineSeries1->append(x_max+50,0);
        x_max+=50;
    }
    else
    {
        xAxis->setRange(-(y_max/2)-50, (y_max/2)+50);
        lineSeries1->append(-(y_max/2)-50,0);
        lineSeries1->append((y_max/2)+50,0);
        x_max = (y_max/2)+50;
    }

    chart->legend()->setVisible(false);
    chartView->chart()->setAxisX(xAxis, lineSeries1);
    chartView->chart()->setAxisY(yAxis, lineSeries1);
    lineSeries2->append(0,100);
    lineSeries2->append(0,-y_max-50);

    chartView->chart()->setAxisX(xAxis, lineSeries2);
    chartView->chart()->setAxisY(yAxis, lineSeries2);
    // конец настройки осей

    chartView->chart()->setAxisX(xAxis, splineSeriesCable1);
    chartView->chart()->setAxisY(yAxis, splineSeriesCable1);
    //получение данных с таблицы
    for (int i = 0; i<count1; i++)
    {
        en1 = 1;
        emit GetData(i);
        while (en1)
        {
            // задержка
        }
        splineSeriesCable1->append(X,Y);
    }

    chartView->chart()->setAxisX(xAxis, splineSeriesCable2);
    chartView->chart()->setAxisY(yAxis, splineSeriesCable2);

    //Рисуем гараж
    double dx = x_max/40;
    double dy = y_max/40;
    lineSeriesGarazh->append(X-dx, Y-dy);
    lineSeriesGarazh->append(X-dx, Y+dy);
    lineSeriesGarazh->append(X+dx, Y+dy);
    lineSeriesGarazh->append(X+dx, Y-dy);
    lineSeriesGarazh->append(X-dx, Y-dy);

    chartView->chart()->setAxisX(xAxis, lineSeriesGarazh);
    chartView->chart()->setAxisY(yAxis, lineSeriesGarazh);

    //нарисовали гараж

    //получение данных из таблицы о кабеле 2
    for (int i = count1; i<count1+count2; i++)
    {
        en1 = 1;
        emit GetData(i);
        while (en1)
        {
            // задержка
        }
        splineSeriesCable2->append(X,Y);
    }

    // Рисуем корабль

    lineSeriesShip->append(0,0);
    lineSeriesShip->append(-(x_max/10),(y_max+100)/20);
    lineSeriesShip->append((3*x_max/10),(y_max+100)/20);
    lineSeriesShip->append((x_max/5),0);
    lineSeriesShip->append(0,0);

    chartView->chart()->setAxisX(xAxis, lineSeriesShip);
    chartView->chart()->setAxisY(yAxis, lineSeriesShip);
    //нарисовали корабль
    //Рисуем ТНПА

    splineSeriesTNPA->append(X, (Y+y_max/50));
    splineSeriesTNPA->append((X+x_max/20),(Y+y_max/80));
    splineSeriesTNPA->append((X+x_max/15),Y);
    splineSeriesTNPA->append((X+x_max/20),(Y-y_max/80));
    splineSeriesTNPA->append(X, (Y-y_max/50));
    splineSeriesTNPA->append((X-x_max/20),(Y-y_max/80));
    splineSeriesTNPA->append((X-x_max/15),Y);
    splineSeriesTNPA->append((X-x_max/20),(Y+y_max/80));
    splineSeriesTNPA->append(X, (Y+y_max/50));

    chartView->chart()->setAxisX(xAxis, splineSeriesTNPA);
    chartView->chart()->setAxisY(yAxis, splineSeriesTNPA);
    //Нарисовали корабль
    //Рисуем векторы скорости

    lineSeriesVx1->append(x_max, -(y_max/6));
    lineSeriesVx2->append(x_max, -(y_max/6)*2);
    lineSeriesVx3->append(x_max, -(y_max/6)*3);
    lineSeriesVx4->append(x_max, -(y_max/6)*4);
    lineSeriesVx5->append(x_max, -(y_max/6)*5);
    lineSeriesVx1->append(x_max-x_max*Vx/10, -(y_max/6));
    lineSeriesVx2->append(x_max-x_max*Vx/10, -(y_max/6)*2);
    lineSeriesVx3->append(x_max-x_max*Vx/10, -(y_max/6)*3);
    lineSeriesVx4->append(x_max-x_max*Vx/10, -(y_max/6)*4);
    lineSeriesVx5->append(x_max-x_max*Vx/10, -(y_max/6)*5);
    double deltax = x_max*Vx/20;
    double deltay = y_max/48;
    lineSeriesVx1->append(x_max-x_max*Vx/10+deltax, -(y_max/6)+deltay);
    lineSeriesVx2->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*2+deltay);
    lineSeriesVx3->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*3+deltay);
    lineSeriesVx4->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*4+deltay);
    lineSeriesVx5->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*5+deltay);
    lineSeriesVx1->append(x_max-x_max*Vx/10, -(y_max/6));
    lineSeriesVx2->append(x_max-x_max*Vx/10, -(y_max/6)*2);
    lineSeriesVx3->append(x_max-x_max*Vx/10, -(y_max/6)*3);
    lineSeriesVx4->append(x_max-x_max*Vx/10, -(y_max/6)*4);
    lineSeriesVx5->append(x_max-x_max*Vx/10, -(y_max/6)*5);
    lineSeriesVx1->append(x_max-x_max*Vx/10+deltax, -(y_max/6)-deltay);
    lineSeriesVx2->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*2-deltay);
    lineSeriesVx3->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*3-deltay);
    lineSeriesVx4->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*4-deltay);
    lineSeriesVx5->append(x_max-x_max*Vx/10+deltax, -(y_max/6)*5-deltay);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx1);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx1);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx2);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx2);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx3);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx3);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx4);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx4);

    chartView->chart()->setAxisX(xAxis, lineSeriesVx5);
    chartView->chart()->setAxisY(yAxis, lineSeriesVx5);
    //Написовали стрелки
    emit SendEnd(0); //Сигнал о завершении рисования

}

void Graphics::Repaint()
{
    delete hlay;
    NewGraphics();

}
