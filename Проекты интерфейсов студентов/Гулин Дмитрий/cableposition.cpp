#include "cableposition.h"
#include "cablecalculation.h"
#include <QEvent>
#include <QKeyEvent>

CablePosition::CablePosition(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    setWindowTitle("Положение кабеля");
    max_Depth = 0;
    connect (widget_3, SIGNAL(GetData(int)), this, SLOT(GetData1(int)));
    connect (widget_3, SIGNAL(SendEnd(bool)), this, SLOT(EndPaint(bool)));
}

void CablePosition::TitleSettings()
{
    widget1->L1=l1;
    widget1->L2=l2;
    widget1->Px=Px;
    widget1->Py=Py;
    widget1->Vx=Vx_TNPA;
    widget1->TitleSettings();
}

void CablePosition::startCalculation()
{
    if (en2)
        Calculation2();
    else
        Calculation1();
}

void CablePosition::Calculation1()
{
    //массивы сил и координат
    int count1 = l1/lotr;
    double cosf[count1];
    double sinf[count1];
    double Rx[count1];
    double Ry[count1];
    double R[count1];
    double Vn[count1];
    double Vt[count1];
    double Rn[count1];
    double Rt[count1];
    double Cn = 1.2;
    double Ct = 0.02;
    double X[count1];
    double Y[count1];

    double x_max = 0;
    double x_min = 0;
    double y_max = 0;
    double y_min = 0;

    double RTNPA = Cvx1*Vx_TNPA*fabs(Vx_TNPA)+Cvx2*Vx_TNPA;

    //Расчет сил, приложенных к первому звену

    R[0] = sqrt(pow((Px-RTNPA),2)+pow((Py-WTNPA),2));
    if (R[0]==0)
    {
        cosf[0]=0;
        sinf[0]=1;
    }
    else
    {
        cosf[0] = (Px-RTNPA)/R[0];
        sinf[0] = (Py-WTNPA)/R[0];
    }
    Vn[0] = Vx_TNPA*sinf[0];
    Vt[0] = Vx_TNPA*cosf[0];
    Rn[0] = Cn*lotr*diam*0.001*500*pow(Vn[0],2);
    Rt[0] = Ct*3.1416*pow(diam, 2)*0.000001*500*0.25*pow(Vt[0],2);

    //Расчет ушлов и приложенных сил для остальных звеньев

    for (int i = 1; i<count1; i++)
    {
        Rx[i] = (R[i-1]-Rt[i-1])*cosf[i-1]-Rn[i-1]*sinf[i-1];
        Ry[i] = (R[i-1]-Rt[i-1])*sinf[i-1]+Rn[i-1]*cosf[i-1]-w1*lotr;
        R[i] = sqrt(pow(Rx[i],2)+pow(Ry[i],2));
        if (R[i]==0)
        {
            cosf[i]=1;
            sinf[i]=0;
        }
        else
        {
            cosf[i] = Rx[i]/R[i];
            sinf[i] = Ry[i]/R[i];
        }
        Vn[i] = Vx_TNPA*sinf[i];
        Vt[i] = Vx_TNPA*cosf[i];
        Rn[i] = Cn*lotr*diam*0.001*500*pow(Vn[i],2);
        Rt[i] = Ct*3.1416*pow(diam, 2)*0.000001*500*0.25*pow(Vt[i],2);
    }

    //Расчет координаты первого звена

    X[0] = lotr*cosf[count1-1];
    Y[0] = lotr*sinf[count1-1];

    //Расчет координат остальных звеньев

    for (int i =1; i<count1; i++)
    {
        X[i] = X[i-1]+lotr*cosf[count1-i-1];
        Y[i] = Y[i-1]+lotr*sinf[count1-i-1];
    }

    for (int i =0; i<count1; i++)
    {
        if (Y[i]<0)
            Y[i] = 0;
        if (max_Depth!=0 && Y[i]>max_Depth)
            Y[i]=max_Depth;
    }

    for (int i = 0; i<count1; i++)
    {
        if (X[i]>x_max)
            x_max=X[i];
        if (X[i]<x_min)
            x_min=X[i];
        if (Y[i]>y_max)
            y_max=Y[i];
        if (Y[i]<y_min)
            y_min=Y[i];
    }

    widget_2->x_max=x_max;
    widget_2->x_min=x_min;
    widget_2->y_max=y_min;
    widget_2->y_min=-y_max;
    widget_2->en2 = en2;

    //Внесение координат в таблицу

    widget_2->count1 = count1;
    widget_2->TableUpdate1();

    for (int i = 0; i<count1; i++)
    {
        widget_2->count = i;
        widget_2->X = X[i];
        widget_2->Y = -Y[i];
        widget_2->UpdateData1();
    }

    Paint1();

}

void CablePosition::Calculation2()
{
    //Массивы первого участка
    int count1 = l1/lotr;
    double cosf1[count1];
    double sinf1[count1];
    double Rx1[count1];
    double Ry1[count1];
    double R1[count1];
    double Vn1[count1];
    double Vt1[count1];
    double Rn1[count1];
    double Rt1[count1];
    double Cn1 = 1.2;
    double Ct1 = 0.02;
    double X1[count1];
    double Y1[count1];

    //Массивы втоого участка
    int count2 = l2/lotr;
    double cosf2[count2];
    double sinf2[count2];
    double Rx2[count2];
    double Ry2[count2];
    double R2[count2];
    double Vn2[count2];
    double Vt2[count2];
    double Rn2[count2];
    double Rt2[count2];
    double Cn2 = 1.2;
    double Ct2 = 0.02;
    double X2[count2];
    double Y2[count2];

    double x_max = 0;
    double x_min = 0;
    double y_max = 0;
    double y_min = 0;

    double RTNPA = Cvx1*Vx_TNPA*fabs(Vx_TNPA)+Cvx2*Vx_TNPA;

    //Начинаем расчет с первого звена второго участка

    R2[0] = sqrt(pow((Px-RTNPA),2)+pow((Py-WTNPA),2));
    if (R2[0]==0)
    {
        cosf2[0]=0;
        sinf2[0]=1;
    }
    else
    {
        cosf2[0] = (Px-RTNPA)/R2[0];
        sinf2[0] = (Py-WTNPA)/R2[0];
    }
    Vn2[0] = Vx_TNPA*sinf2[0];
    Vt2[0] = Vx_TNPA*cosf2[0];
    Rn2[0] = Cn2*lotr*diam*0.001*500*pow(Vn2[0],2);
    Rt2[0] = Ct2*3.1416*pow(diam, 2)*0.000001*500*0.25*pow(Vt2[0],2);

    //Расчет сил и углов для остальных звеньев второго участка

    for (int i = 1; i<count2; i++)
    {
        Rx2[i] = (R2[i-1]-Rt2[i-1])*cosf2[i-1]-Rn2[i-1]*sinf2[i-1];
        Ry2[i] = (R2[i-1]-Rt2[i-1])*sinf2[i-1]+Rn2[i-1]*cosf2[i-1]-w2*lotr;
        R2[i] = sqrt(pow(Rx2[i],2)+pow(Ry2[i],2));
        if (R2[i]==0)
        {
            cosf2[i]=1;
            sinf2[i]=0;
        }
        else
        {
            cosf2[i] = Rx2[i]/R2[i];
            sinf2[i] = Ry2[i]/R2[i];;
        }
        Vn2[i] = Vx_TNPA*sinf2[i];
        Vt2[i] = Vx_TNPA*cosf2[i];
        Rn2[i] = Cn2*lotr*diam*0.001*500*pow(Vn2[i],2);
        Rt2[i] = Ct2*3.1416*pow(diam, 2)*0.000001*500*0.25*pow(Vt2[i],2);
    }

    //Расчет первого звена первого участка

    Rx1[0] = (R2[count2-1]-Rt2[count2-1])*cosf2[count2-1]-Rn2[count2-1]*sinf2[count2-1];
    Ry1[0] = (R2[count2-1]-Rt2[count2-1])*sinf2[count2-1]+Rn2[count2-1]*cosf2[count2-1]+w2*lotr-wgarazh;
    R1[0] = sqrt(pow(Rx1[0],2)+pow(Ry1[0],2));
    if (R1[0]==0)
    {
        cosf1[0]=1;
        sinf1[0]=0;
    }
    else
    {
        cosf1[0] = Rx1[0]/R1[0];
        sinf1[0] = Ry1[0]/R1[0];
    }
    Vn1[0] = Vx_TNPA*sinf1[0];
    Vt1[0] = Vx_TNPA*cosf1[0];
    Rn1[0] = Cn1*lotr*diam*0.001*500*pow(Vn1[0],2);
    Rt1[0] = Ct1*3.1416*pow(diam, 2)*0.000001*500*0.25*pow(Vt1[0],2);

    //Расчет остальных звеньев первого участка

    for (int i = 1; i<count1; i++)
    {
        Rx1[i] = (R1[i-1]-Rt1[i-1])*cosf1[i-1]-Rn1[i-1]*sinf1[i-1];
        Ry1[i] = (R1[i-1]-Rt1[i-1])*sinf1[i-1]+Rn1[i-1]*cosf1[i-1]-w1*lotr;
        R1[i] = sqrt(pow(Rx1[i],2)+pow(Ry1[i],2));
        if (R1[i]==0)
        {
            cosf1[i]=1;
            sinf1[i]=0;
        }
        else
        {
            cosf1[i] = Rx1[i]/R1[i];
            sinf1[i] = Ry1[i]/R1[i];
        }
        Vn1[i] = Vx_TNPA*sinf1[i];
        Vt1[i] = Vx_TNPA*cosf1[i];
        Rn1[i] = Cn2*lotr*diam*0.001*500*pow(Vn1[i],2);
        Rt1[i] = Ct2*3.1416*pow(diam, 2)*0.000001*500*0.25*pow(Vt1[i],2);
    }

    //Расчет координат начинается с первого звена первого участка

    X1[0] = lotr*cosf1[count1-1];
    Y1[0] = lotr*sinf1[count1-1];

    //Координаты остальных звеньев первого участка

    for (int i =1; i<count1; i++)
    {
        X1[i] = X1[i-1]+lotr*cosf1[count1-i-1];
        Y1[i] = Y1[i-1]+lotr*sinf1[count1-i-1];
    }

    for (int i =0; i<count1; i++)
    {
        if (Y1[i]<0)
            Y1[i] = 0;
        if (max_Depth!=0 && Y1[i]>max_Depth)
            Y1[i]=max_Depth;
    }

    //Расчет координат первого звена второго участка

    X2[0] = X1[count1-1]+lotr*cosf2[count2-1];
    Y2[0] = Y1[count1-1]+lotr*sinf2[count2-1];

    //Координаты остальных звеньев второго участка

    for (int i =1; i<count2; i++)
    {
        X2[i] = X2[i-1]+lotr*cosf2[count2-i-1];
        Y2[i] = Y2[i-1]+lotr*sinf2[count2-i-1];
    }

    for (int i =0; i<count2; i++)
    {
        if (Y2[i]<0)
            Y2[i] = 0;
        if (max_Depth!=0 && Y2[i]>max_Depth)
            Y2[i]=max_Depth;
    }

    for (int i = 0; i<count1; i++)
    {
        if (X1[i]>x_max)
            x_max=X1[i];
        if (X1[i]<x_min)
            x_min=X1[i];
        if (Y1[i]>y_max)
            y_max=Y1[i];
        if (Y1[i]<y_min)
            y_min=Y1[i];
    }

    for (int i = 0; i<count2; i++)
    {
        if (X2[i]>x_max)
            x_max=X2[i];
        if (X2[i]<x_min)
            x_min=X2[i];
        if (Y2[i]>y_max)
            y_max=Y2[i];
        if (Y2[i]<y_min)
            y_min=Y2[i];
    }

    widget_2->x_max=x_max;
    widget_2->x_min=x_min;
    widget_2->y_max= y_min;
    widget_2->y_min=-y_max;
    widget_2->en2 = en2;

    //Внесение координат в таблицу

    widget_2->count1 = count1;
    widget_2->count2 = count2;
    widget_2->TableUpdate2();

    for (int i = 0; i<count1; i++)
    {
        widget_2->count = i;
        widget_2->X = X1[i];
        widget_2->Y = -Y1[i];
        widget_2->UpdateData1();
    }

    for (int i = 0; i<count2; i++)
    {
        widget_2->count = i;
        widget_2->X = X2[i];
        widget_2->Y = -Y2[i];
        widget_2->UpdateData2();
    }

    Paint1();

}

void CablePosition::Paint1()
{
    widget_3->x_max = widget_2->x_max;
    widget_3->x_min = widget_2->x_min;
    widget_3->y_max = widget_2->y_max;
    widget_3->y_min = widget_2->y_min;
    widget_3->count1 = l1/lotr;
    widget_3->count2 = l2/lotr;
    widget_3->en2 = en2;
    widget_3->Vx = Vx_TNPA;
    widget_3->StartPaint();
}

void CablePosition::GetData1(int i)
{
    widget_3->X = widget_2->GetData(i, 0);
    widget_3->Y = widget_2->GetData(i, 1);
    widget_3->en1 = 0;
}

void CablePosition::EndPaint(bool a)
{
    en_repain = a;
}

/*void CablePosition::paintEvent(QPaintEvent *e)//Рисуем кабель
{
        QPainter painter(this);//указываем на поле
        int w = width();
        int h = height();
        w = (w-350)/2;
        h = h-150;
        if (fabs(x_min)>fabs(x_max))
            x_max = x_min;
        double kx = w/(fabs(x_max));
        double ky = h/(fabs(y_max)+fabs(y_min));
        double k;
        if (kx>ky)
            k=ky;
        else
            k=kx;
        painter.setRenderHint(QPainter::Antialiasing,true);
        //painter.translate(50+fabs(x_min)*k, 100);
        painter.translate((width()-250)/2, 100);
        painter.setPen(Qt::blue);
        painter.save();
        int count1 = l1/lotr;
        double X, Y, X_Prev, Y_Prev;
        X_Prev = Y_Prev = 0;
        X = widget_2->GetData(0, 0)*k;
        Y = widget_2->GetData(0, 1)*k;
        painter.drawLine(X_Prev,Y_Prev,X,Y);
        for (int i = 1; i<count1; i++)
        {
            X = widget_2->GetData(i, 0)*k;
            Y = widget_2->GetData(i, 1)*k;
            X_Prev = widget_2->GetData(i-1, 0)*k;
            Y_Prev = widget_2->GetData(i-1, 1)*k;
            painter.drawLine(X_Prev,Y_Prev,X,Y);
        }
        if (en2)
        {
            painter.setPen(Qt::green);
            int count2 = l2/lotr;
            X_Prev = widget_2->GetData(count1-1, 0);
            Y_Prev = widget_2->GetData(count1-1, 1);
            X = widget_2->GetData(count1, 0)*k;
            Y = widget_2->GetData(count1, 1)*k;
            //painter.drawLine(X_Prev,Y_Prev,X,Y);
            for (int i = 1; i<count2; i++)
            {
                X = widget_2->GetData(count1+i, 0)*k;
                Y = widget_2->GetData(count1+i, 1)*k;
                X_Prev = widget_2->GetData(count1+i-1, 0)*k;
                Y_Prev = widget_2->GetData(count1+i-1, 1)*k;
                painter.drawLine(X_Prev,Y_Prev,X,Y);
            }
        }
        painter.end();
        widget_3->show();
}*/

void CablePosition::UpdateGraph()
{
    TitleSettings();
    while (en_repain)
    {
        setCursor(Qt::WaitCursor);
    }
    setCursor(Qt::ArrowCursor);
    en_repain = 1;
    widget_3->Repaint();
    startCalculation();
}


void CablePosition::on_L1stepminus_clicked()
{
    L1step->setText(QString::number(L1step->text().toDouble()/10));
}

void CablePosition::on_L1stepplus_clicked()
{
    L1step->setText(QString::number(L1step->text().toDouble()*10));
}

void CablePosition::on_L2stepminus_clicked()
{
    L2step->setText(QString::number(L2step->text().toDouble()/10));
}

void CablePosition::on_L2stepplus_clicked()
{
    L2step->setText(QString::number(L2step->text().toDouble()*10));
}

void CablePosition::on_Pxstepminus_clicked()
{
    Pxstep->setText(QString::number(Pxstep->text().toDouble()/10));
}

void CablePosition::on_Pxstepplus_clicked()
{
    Pxstep->setText(QString::number(Pxstep->text().toDouble()*10));
}

void CablePosition::on_Pystepminus_clicked()
{
    Pystep->setText(QString::number(Pystep->text().toDouble()/10));
}

void CablePosition::on_Pystepplus_clicked()
{
    Pystep->setText(QString::number(Pystep->text().toDouble()*10));
}

void CablePosition::on_Vxstepminus_clicked()
{
    Vxstep->setText(QString::number(Vxstep->text().toDouble()/10));
}

void CablePosition::on_Vxstepplus_clicked()
{
    Vxstep->setText(QString::number(Vxstep->text().toDouble()*10));
}

void CablePosition::on_L1minus_clicked()
{
    l1=l1-(L1step->text().toDouble());
    UpdateGraph();
}

void CablePosition::on_L1zero_clicked()
{
    l1=0;
    UpdateGraph();
}

void CablePosition::on_L1plus_clicked()
{
    l1=l1+(L1step->text().toDouble());
    UpdateGraph();
}

void CablePosition::on_L2minus_clicked()
{
    l2=l2-(L2step->text().toDouble());
    if (l2==0)
        en2=0;
    UpdateGraph();
}

void CablePosition::on_L2zero_clicked()
{
    l2=0;
    en2=0;
    UpdateGraph();
}

void CablePosition::on_L2plus_clicked()
{
    l2=l2+(L2step->text().toDouble());
    en2=1;
    UpdateGraph();
}

void CablePosition::on_Pxminus_clicked()
{
    Px=Px-(Pxstep->text().toDouble());
    UpdateGraph();
}

void CablePosition::on_Pxzero_clicked()
{
    Px=0;
    UpdateGraph();
}

void CablePosition::on_Pxplus_clicked()
{
    Px=Px+(Pxstep->text().toDouble());
    UpdateGraph();
}

void CablePosition::on_Pyminus_clicked()
{
    Py=Py-(Pystep->text().toDouble());
    UpdateGraph();
}

void CablePosition::on_Pyzero_clicked()
{
    Py=0;
    UpdateGraph();
}

void CablePosition::on_Pyplus_clicked()
{
    Py=Py+(Pystep->text().toDouble());
    UpdateGraph();
}

void CablePosition::on_Vxminus_clicked()
{
    Vx_TNPA=Vx_TNPA-(Vxstep->text().toDouble());
    UpdateGraph();
}


void CablePosition::on_Vxzero_clicked()
{
    Vx_TNPA=0;
    UpdateGraph();
}

void CablePosition::on_Vxplus_clicked()
{
    Vx_TNPA=Vx_TNPA+(Vxstep->text().toDouble());
    UpdateGraph();
}

void CablePosition::on_toolButton_2_clicked()
{
    if (en_left)
    {
        groupBox->hide();
        groupBox_2->hide();
        groupBox_3->hide();
        groupBox_4->hide();
        groupBox_5->hide();
        toolButton_2->setArrowType(Qt::RightArrow);
        en_left = 0;
    }
    else
    {
        groupBox->show();
        groupBox_2->show();
        groupBox_3->show();
        groupBox_4->show();
        groupBox_5->show();
        toolButton_2->setArrowType(Qt::LeftArrow);
        en_left = 1;
    }
}

void CablePosition::on_toolButton_clicked()
{
    if (en_right)
    {
        widget_2->hide();
        toolButton->setArrowType(Qt::LeftArrow);
        en_right = 0;
    }
    else
    {
        widget_2->show();
        toolButton->setArrowType(Qt::RightArrow);
        en_right = 1;
    }
}
