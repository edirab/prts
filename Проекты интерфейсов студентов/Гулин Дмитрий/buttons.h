#ifndef BUTTONS_H
#define BUTTONS_H

#include "ui_buttons.h"

class Buttons : public QWidget, private Ui::Buttons
{
    Q_OBJECT

public:
    explicit Buttons(QWidget *parent = nullptr);
    bool enButton1=0;
signals:
    void button1_clicked();
};

#endif // BUTTONS_H
