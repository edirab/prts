#include "cableposition_table.h"

cableposition_table::cableposition_table(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
}

void cableposition_table::TableUpdate1()
{
    tableWidget->setRowCount(count1);
    for (int i = 0; i < tableWidget->rowCount(); ++i)
        for (int j = 0; j < tableWidget->columnCount(); ++j) {
         if (tableWidget->item(i, j) == 0)
         tableWidget->setItem(i, j, new QTableWidgetItem());
         tableWidget->item(i, j)->setTextAlignment(Qt::AlignHCenter);
         tableWidget->item(i,j)->setBackgroundColor("lightblue");
        }
}

void cableposition_table::UpdateData1()
{
    tableWidget->item(count, 0)->setText(QString::number(X));
    tableWidget->item(count, 1)->setText(QString::number(Y));
}

void cableposition_table::TableUpdate2()
{
    tableWidget->setRowCount(count1+count2);
    for (int i = 0; i < tableWidget->rowCount(); ++i)
        for (int j = 0; j < tableWidget->columnCount(); ++j) {
         if (tableWidget->item(i, j) == 0)
         tableWidget->setItem(i, j, new QTableWidgetItem());
         tableWidget->item(i, j)->setTextAlignment(Qt::AlignHCenter);
         if (i<count1)
            tableWidget->item(i,j)->setBackgroundColor("lightblue");
         else
            tableWidget->item(i,j)->setBackgroundColor("lightgreen");
        }
}

void cableposition_table::UpdateData2()
{
    tableWidget->item(count+count1, 0)->setText(QString::number(X));
    tableWidget->item(count+count1, 1)->setText(QString::number(Y));
}

double cableposition_table::GetData(int i, int j)
{
    return(tableWidget->item(i,j)->text().toDouble());
}
