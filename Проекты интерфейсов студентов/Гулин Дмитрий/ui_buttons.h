/********************************************************************************
** Form generated from reading UI file 'buttons.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BUTTONS_H
#define UI_BUTTONS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Buttons
{
public:
    QHBoxLayout *horizontalLayout;
    QPushButton *Button1;
    QPushButton *Button2;

    void setupUi(QWidget *Buttons)
    {
        if (Buttons->objectName().isEmpty())
            Buttons->setObjectName(QStringLiteral("Buttons"));
        Buttons->resize(620, 62);
        Buttons->setMinimumSize(QSize(620, 62));
        Buttons->setMaximumSize(QSize(16777215, 62));
        horizontalLayout = new QHBoxLayout(Buttons);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        Button1 = new QPushButton(Buttons);
        Button1->setObjectName(QStringLiteral("Button1"));
        Button1->setMinimumSize(QSize(291, 41));
        Button1->setStyleSheet(QLatin1String("background-color:lightblue;\n"
"font-size:20px;"));

        horizontalLayout->addWidget(Button1);

        Button2 = new QPushButton(Buttons);
        Button2->setObjectName(QStringLiteral("Button2"));
        Button2->setMinimumSize(QSize(291, 41));
        Button2->setStyleSheet(QLatin1String("background-color:lightblue;\n"
"font-size:20px;"));

        horizontalLayout->addWidget(Button2);


        retranslateUi(Buttons);

        QMetaObject::connectSlotsByName(Buttons);
    } // setupUi

    void retranslateUi(QWidget *Buttons)
    {
        Buttons->setWindowTitle(QApplication::translate("Buttons", "Form", nullptr));
        Button1->setText(QApplication::translate("Buttons", "\320\240\320\260\321\201\321\201\321\207\320\270\321\202\320\260\321\202\321\214 \320\277\320\276\320\273\320\276\320\266\320\265\320\275\320\270\320\265 \320\272\320\260\320\261\320\265\320\273\321\217", nullptr));
        Button2->setText(QApplication::translate("Buttons", "\320\240\320\260\321\201\321\201\321\207\320\270\321\202\320\260\321\202\321\214 \321\200\320\260\320\261\320\276\321\207\321\203\321\216 \320\267\320\276\320\275\321\203", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Buttons: public Ui_Buttons {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BUTTONS_H
