#ifndef CABLEPOSITION_1_H
#define CABLEPOSITION_1_H

#include "ui_cableposition_1.h"

class cableposition_1 : public QWidget, private Ui::cableposition_1
{
    Q_OBJECT

public:
    explicit cableposition_1(QWidget *parent = nullptr);
    double L1, L2, Px, Py, Vx;
    void TitleSettings();
};

#endif // CABLEPOSITION_1_H
