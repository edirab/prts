#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <QWidget>
#include "ui_graphics.h"
#include <QtCharts>
#include <QHBoxLayout>

using namespace QtCharts;

class Graphics : public QWidget, private Ui::Graphics
{
    Q_OBJECT

public:
    explicit Graphics(QWidget *parent = nullptr);
    ~Graphics();
    double x_max, x_min, y_max, y_min;
    int count1, count2;
    bool en2;
    bool en1;
    double X, Y;
    double Vx;

    void StartPaint();
    void Repaint();
    void NewGraphics();

signals:
    void GetData(int);
    void SendEnd(bool);

private:
    void Paint1();
    void Paint2();

    QChartView * chartView;
    QSplineSeries * splineSeriesCable1;
    QSplineSeries * splineSeriesCable2;
    QLineSeries * lineSeriesShip;
    QLineSeries * lineSeriesGarazh;
    QSplineSeries * splineSeriesTNPA;
    QLineSeries * lineSeriesVx1;
    QLineSeries * lineSeriesVx2;
    QLineSeries * lineSeriesVx3;
    QLineSeries * lineSeriesVx4;
    QLineSeries * lineSeriesVx5;
    QLineSeries * lineSeries1;
    QLineSeries * lineSeries2;
    QChart * chart;
    QValueAxis * xAxis;
    QValueAxis * yAxis;
    QHBoxLayout * hlay;
};

#endif // GRAPHICS_H
